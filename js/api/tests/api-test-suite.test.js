/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { server } from './mocks/server';

describe('APIs', () => {
  beforeAll(() => server.listen({ onUnhandledRequest: 'error' }));
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  require('./eclipsefdn.adopters.test');
  require('./eclipsefdn.interest-groups.test');
  require('./eclipsefdn.newsroom.test');
  require('./eclipsefdn.media-link.test');
  require('./eclipsefdn.membership.test');
  require('./eclipsefdn.projects.test');
  require('./utils.test');
});
