/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { getNewsroomResources, getAds } from '../eclipsefdn.newsroom';

describe('Newsroom API', () => {
  describe('Newsroom Resources', () => {
    it('Should return a list of resources', async () => {
      const [newsroomResources, error] = await getNewsroomResources();
      expect(error).toBeNull();
      expect(newsroomResources).not.toBeNull();
    });

    it('Should return a list of a specified resource type from the `resourceTypes` parameter', async () => {
      const [caseStudies] = await getNewsroomResources({ 
        resourceTypes: ['case_study'] 
      });
      const [whitePapers] = await getNewsroomResources({
        resourceTypes: ['white_paper']
      });

      expect(caseStudies.length).toBeGreaterThan(0);
      expect(whitePapers.length).toBeGreaterThan(0);
      expect(caseStudies.every(r => r.resourceType.includes('case_study'))).toBe(true);
      expect(whitePapers.every(r => r.resourceType.includes('white_paper'))).toBe(true);
    });

    it('Should return a list of resources of a specified publishing location from the `publishTo` parameter', async () => {
      const [newsroomResources] = await getNewsroomResources({ 
        publishTo: ['eclipse_org']
      });

      expect(newsroomResources.length).toBeGreaterThan(0);
      expect(newsroomResources.every(r => r.publishTo.includes('eclipse_org'))).toBe(true);
    });

    it('Allow for multiple `publishTo` parameters', async () => {
      const [newsroomResources] = await getNewsroomResources({ 
        publishTo: ['eclipse_org', 'sparkplug']
      });

      expect(newsroomResources.every(r => r.publishTo.includes('eclipse_org') || r.publishTo.includes('sparkplug'))).toBe(true);
      expect(newsroomResources.find(r => r.publishTo.includes('eclipse_org'))).not.toBeNull();
      expect(newsroomResources.find(r => r.publishTo.includes('sparkplug'))).not.toBeNull();
    });

    it('Limit the number of resources with the `pageSize` parameter', async () => {
      const [newsroomResources] = await getNewsroomResources({
        pageSize: 3
      });

      expect(newsroomResources).toHaveLength(3);
    });
  });
  
  describe('Ads', () => {
    it('Should return a list of ads', async () => {
      const [ads, error] = await getAds({
        publishTo: 'eclipse_org_home',
        formats: { 'ads_square': '1' }
      });

      expect(error).toBeNull();
      expect(ads).not.toBeNull();
      expect(ads).not.toHaveLength(0);
      expect(ads.every(ad => ad.publishTo.includes('eclipse_org_home'))).toBe(true);
    });

    it('Should return a list of ads for a specific website or webpage', async () => {
      let ads;

      // Ads for the eclipse.org homepage
      [ads] = await getAds({
        publishTo: 'eclipse_org_home',
        formats: { 'ads_square': '1' }
      });

      expect(ads).not.toBeNull();
      expect(ads.every(ad => ad.publishTo.includes('eclipse_org_home'))).toBe(true);

      // Ads for the eclipse.org/downloads page
      [ads] = await getAds({
        publishTo: 'eclipse_org_downloads',
        formats: { 'ads_square': '1' }
      });

      expect(ads).not.toBeNull();
      expect(ads.every(ad => ad.publishTo.includes('eclipse_org_downloads'))).toBe(true);
    });
  });
});
