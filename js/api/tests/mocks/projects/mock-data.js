/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { faker } from '@faker-js/faker';

const industryCollaborations = ['Jakarta EE', 'Internet of Things', 'Sparkplug'];

const generateIndustryCollaboration = (collaborationName) => ({
  name: collaborationName,
  id: collaborationName.toLowerCase().replaceAll(' ', '-'),
});

/**
  * Generate a mock project.
  *
  * @param {Object} [options] - Options to customize the mock project.
  * @param {string} [options.state] - The state of the project.
  * @param {string} [options.industryCollaboration] - The industry collaboration name of the project.
  */
export const generateMockProject = (options) => {
  const topLevelName = faker.commerce.productName();
  const name = faker.commerce.productName();
  const shortId = name.toLowerCase().replaceAll(' ', '-');
  const projectId = `${topLevelName.toLowerCase().replaceAll(' ', '-')}.${shortId}`;

  return {
    project_id: projectId,
    short_project_id: shortId,
    name: `${topLevelName} ${name}`,
    summary: faker.lorem.paragraph(),
    url: `https://projects.eclipse.org/projects/${projectId}`,
    website_url: faker.internet.url(),
    website_repo: [],
    logo: faker.image.urlPlaceholder(),
    tags: [],
    github_repos: [],
    github: {
      org: '',
      ignored_repos: [],
    },
    gitlab_repos: [],
    gitlab: {
      project_group: '',
      ignored_sub_groups: [],
    },
    gerrit_repos: [],
    contributors: [],
    committers: [],
    project_leads: [],
    working_groups: [],
    industry_collaborations: options?.industryCollaboration 
      // If an industry collaboration is specified, use it.
      ? [generateIndustryCollaboration(options.industryCollaboration)]
      // Otherwise, use a random one.
      : [faker.helpers.arrayElements(industryCollaborations.map(generateIndustryCollaboration))],
    spec_project_working_group: [],
    state: options?.state || faker.helpers.arrayElement(['Regular', 'Archived']),
    releases: [
      { name: '1.0.0', date: '2020-01-01', url: faker.internet.url(), type: '2' },
    ],
    top_level_project: topLevelName.toLowerCase(),
    slsa_level: '',
  }
}

const generateMockProposal = () => {
  const name = faker.commerce.productName();
  const parent = faker.commerce.productName().toLowerCase().replaceAll(' ', '-');
  const projectId = `${parent}.${name.toLowerCase().replaceAll(' ', '-')}`;

  return {
    id: faker.number.int(),
    project_id: projectId,
    name,
    parent, 
    top_level_project: parent,
    industry_collaboration: faker.helpers.arrayElement(industryCollaborations.map(generateIndustryCollaboration)),
    state: faker.helpers.arrayElement(['Created', 'Draft']),
    summary: faker.lorem.paragraph(),
    scope: faker.lorem.paragraph(),
    background: faker.lorem.paragraph(),
    url: `https://projects.eclipse.org/proposals/${name.toLowerCase().replaceAll(' ', '-')}`,
    project_url: `https://projects.eclipse.org/projects/${projectId}`,
    spec_project: [],
    committers: [],
    project_leads: [],
    mentors: [],
    patent_license: '',
    licenses: ['Eclipse Public License 2.0'],
    repo_type: null,
    repos: []
  };
};

export const mockProjects = Array.from({ length: 10 }, () => generateMockProject())
  // Ensure that there is at least one project for each state. This is to
  // ensure that tests that rely on a specific state will not fail.
  .concat(generateMockProject({ state: 'Archived' }))
  .concat(generateMockProject({ state: 'Proposal' }))
  .concat(generateMockProject({ state: 'Regular' }))
  .concat(
    Array.from({ length: 5 }, () => generateMockProject({ industryCollaboration: 'Jakarta EE' }))
  );

export const mockProposals = Array.from({ length: 10 }, () => generateMockProposal());
