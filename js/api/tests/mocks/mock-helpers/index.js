/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const buildMockPaginationLinkHeader = (req, hasMore = true) => {
  const selfUrl = new URL(req.url);
  const currentPage = +selfUrl.searchParams.get('page') || 1;

  let linkHeader = `<${selfUrl.toString()}>; rel="self"`;
  if (!hasMore) {
    return linkHeader;
  }

  // If there is more pages of data
  const nextUrl = new URL(req.url);
  nextUrl.searchParams.set('page', currentPage + 1);

  linkHeader += `, <${nextUrl.toString()}>; rel="next"`;

  return linkHeader;
};
