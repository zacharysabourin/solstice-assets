/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// No personal data used in this mock data. Names and usernames have been
// generated.

export const mockAds = [
  {
    id: '38615',
    campaign_name: '38615',
    url: 'https://go.zenhub.com/long-ppc/eclipse?utm_source=eclipse-\u0026amp;utm_medium=cpc\u0026amp;utm_campaign=dp-paid-listing-eclipse',
    image:
    'https://newsroom.eclipse.org/sites/default/files/ads/1.%20eclipse_ab-test_v1-300x250-1_manage-learnmore.jpg',
    member_organization_id: '',
    type: 'paid',
    format: 'ads_square',
    weight: '43',
    groups: '',
    publish_to: [],
  },
  {
    id: '38553',
    campaign_name: '38553',
    url: 'https://go.zenhub.com/long-ppc/eclipse?utm_source=eclipse-\u0026amp;utm_medium=cpc\u0026amp;utm_campaign=dp-paid-listing-eclipse',
    image:
    'https://newsroom.eclipse.org/sites/default/files/ads/B%20test%20eclipse_ab-test_v1-728x90-3_bestway_noGH-learnmore_0.jpg',
    member_organization_id: '',
    type: 'paid',
    format: 'ads_leaderboard',
    weight: '10',
    groups: '',
    publish_to: [],
  },
  {
    id: '38552',
    campaign_name: '38552',
    url: 'https://go.zenhub.com/long-ppc/eclipse?utm_source=eclipse-\u0026amp;utm_medium=cpc\u0026amp;utm_campaign=dp-paid-listing-eclipse',
    image:
    'https://newsroom.eclipse.org/sites/default/files/ads/A%20test-eclipse_ab-test_v1-728x90-3_bestway-learnmore_0.jpg',
    member_organization_id: '',
    type: 'paid',
    format: 'ads_leaderboard',
    weight: '10',
    groups: '',
    publish_to: [],
  },
  {
    id: '38551',
    campaign_name: '38551',
    url: 'https://go.zenhub.com/long-ppc/eclipse?utm_source=eclipse-\u0026amp;utm_medium=cpc\u0026amp;utm_campaign=dp-paid-listing-eclipse',
    image:
    'https://newsroom.eclipse.org/sites/default/files/ads/B%20test%20eclipse_ab-test_v1-728x90-3_bestway_noGH-learnmore.jpg',
    member_organization_id: '',
    type: 'paid',
    format: 'ads_top_leaderboard',
    weight: '35',
    groups: '',
    publish_to: [],
  },
  {
    id: '38550',
    campaign_name: '38550',
    url: 'https://go.zenhub.com/long-ppc/eclipse?utm_source=eclipse-\u0026amp;utm_medium=cpc\u0026amp;utm_campaign=dp-paid-listing-eclipse',
    image:
    'https://newsroom.eclipse.org/sites/default/files/ads/A%20test-eclipse_ab-test_v1-728x90-3_bestway-learnmore.jpg',
    member_organization_id: '',
    type: 'paid',
    format: 'ads_top_leaderboard',
    weight: '35',
    groups: '',
    publish_to: [],
  },
  {
    id: '38499',
    campaign_name: '38499',
    url: 'https://www.oracle.com/tools/downloads/oepe-v12219-downloads.html',
    image: 'https://newsroom.eclipse.org/sites/default/files/ads/oepe_ad_200x200_0.jpg',
    member_organization_id: '',
    type: 'strat_ad',
    format: 'ads_square',
    weight: '12',
    groups: '',
    publish_to: ['eclipse_org_downloads'],
  },
  {
    id: '38497',
    campaign_name: '38497',
    url: 'https://www.crystalreports.com/?source=ppc-global-google-cr\u0026amp;gclid=EAIaIQobChMIuLWaouK44AIVDVmGCh0jEAcHEAAYASAAEgIc2_D_BwE\u0026amp;gclsrc=aw.ds',
    image: 'https://newsroom.eclipse.org/sites/default/files/ads/sap200x200_0.jpg',
    member_organization_id: '',
    type: 'strat_ad',
    format: 'ads_square',
    weight: '12',
    groups: '',
    publish_to: ['eclipse_org_downloads'],
  },
  {
    id: '38336',
    campaign_name: '38336',
    url: 'https://developer.huaweicloud.com/en-us/',
    image: 'https://newsroom.eclipse.org/sites/default/files/ads/huawei-ad-eclipse-hdc.jpeg',
    member_organization_id: '',
    type: 'strat_ad',
    format: 'ads_square',
    weight: '12',
    groups: '',
    publish_to: ['eclipse_org_home'],
  },
];

export const mockNewsroomResources = [
  {
    "id": "39384",
    "title": "Eclipse BaSyx Bridges the Middleware Gap for Industry 4.0",
    "date": "2023-03-10T14:44:49Z",
    "body": "Learn how the Eclipse Foundation is enabling the creation of Industry 4.0 solutions.",
    "direct_link": "https://outreach.eclipse.foundation/industry40-middleware-basyx",
    "landing_link": "https://outreach.eclipse.foundation/industry40-middleware-basyx",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/Eclipse%20BaSyx%20Thumb.png?itok=cvv-GHcf",
    "resource_type": "case_study",
    "author": {
      "full_name": "Sandi Lough",
      "username": "slough0"
    },
    "publish_to": [
      "eclipse_org",
      "research"
    ]
  },
  {
    "id": "38479",
    "title": "From DevOps to EdgeOps: A Vision for Edge Computing",
    "date": "2021-10-07T18:35:01Z",
    "body": "",
    "direct_link": "https://f.hubspotusercontent10.net/hubfs/5413615/Eclipse%20Foundation%20EdgeOps%20White%20Paper.pdf",
    "landing_link": "https://outreach.eclipse.foundation/edge-computing-edgeops-white-paper",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/from-devops-to-edgeops.jpg?itok=slWEDQ-e",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Anthony Spollen",
      "username": "aspollen8"
    },
    "publish_to": [
      "eclipse_org",
      "edge_native"
    ]
  },
  {
    "id": "38478",
    "title": "OpenADX Manifesto",
    "date": "2021-10-07T18:32:40Z",
    "body": "",
    "direct_link": "https://openadx.eclipse.org/resources/OpenADx-Manifesto-v12-2020.pdf",
    "landing_link": "",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/OpenADX.PNG?itok=aerORg6y",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Mic Santino",
      "username": "msantiop"
    },
    "publish_to": [
      "eclipse_org",
      "openadx"
    ]
  },
  {
    "id": "39381",
    "title": "Sparkplug: The Open Specification Critical to Achieving ROI in the Industrial Internet of Things",
    "date": "2023-03-08T12:12:51Z",
    "body": "",
    "direct_link": "https://www.hivemq.com/solutions/hivemq-mqtt-sparkplug-open-specification-for-iiot-whitepaper/",
    "landing_link": "",
    "image": "https://newsroom.eclipse.org/sites/default/files/styles/large/public/resources/images/Untitled%20design.png?itok=uDycYlCg",
    "resource_type": "white_paper",
    "author": {
      "full_name": "Anonymous",
      "username": ""
    },
    "publish_to": [
      "sparkplug"
    ]
  }
]
