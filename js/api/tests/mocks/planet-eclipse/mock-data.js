import { faker } from '@faker-js/faker';

const generateMockPost = () => {
  const postUrl = faker.internet.url();
  return `
    <item>
      <title>${faker.person.fullName()}: ${faker.lorem.sentence(4)}</title>
      <guid isPermaLink="true">${postUrl}</guid>
      <link>${postUrl}</link>
      <description>
        ${faker.lorem.paragraphs()}
      </description>
      <pubDate>Wed, 07 Feb 2024 19:12:47 +0000</pubDate>
    </item>`;
};

// Note that the atom:link element has been renamed to atom. Otherwise, it
// would not parse properly. The prefix is not working for this mock. 
export const mockFeed = `
  <rss version="2.0">
    <channel>
      <title>Planet Eclipse</title>
      <link>http://planeteclipse.org/</link>
      <language>en</language>
      <description>Planet Eclipse - http://planeteclipse.org/</description>
      <atom rel="self" href="http://planeteclipse.org/rss20.xml" type="application/rss+xml"/>
      ${Array.from({length: 10 }).map(() => generateMockPost()).join('')}
    </channel>
  </rss>
`;

