/*
 * Copyright (c) 2024 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { rest } from 'msw';
import { mockFeed } from './mock-data';
import { buildMockPaginationLinkHeader } from '../mock-helpers';

const apiUrl = 'https://planeteclipse.org/planet';

const handlers = [
  rest.get(`${apiUrl}/rss20.xml`, (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.xml(mockFeed),
    );
  })
];

export default handlers;
