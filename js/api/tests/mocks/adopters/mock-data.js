/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { faker } from '@faker-js/faker';
import { transformSnakeToCamel } from '../../../utils';


const generateMockAdopters = () => {
  return {
    homepage_url: faker.internet.url(),
    logo: faker.image.urlPlaceholder(),
    logo_white: faker.image.urlPlaceholder(),
    name: faker.company.name(),
  };
};

export const workingGroupProjects = {
  'jakarta-ee': new Set(),
  'internet-things-iot': new Set(),
  'sparkplug': new Set(),
};

const generateMockProjectAdopters = () => {
  const name = faker.commerce.productName();
  const projectId = name.toLowerCase().replaceAll(' ', '-');
  const workingGroups = Object.keys(workingGroupProjects)
  const randomlySelectedWorkingGroup = workingGroups[faker.number.int({ min: 0, max: workingGroups.length - 1 })];
  workingGroupProjects[randomlySelectedWorkingGroup].add(projectId);

  return {
    name,
    adopters: Array.from({ length: faker.number.int({ min: 1, max: 5 }) }, () => generateMockAdopters()),
    logo: faker.image.urlPlaceholder(),
    project_id: projectId,
    url: faker.internet.url(),
  }
};

const mockProjectAdoptersResponses = Array.from({ length: faker.number.int({ min: 10, max: 20 }) }, () => generateMockProjectAdopters());

export const mockProjectAdopters = {
  api: mockProjectAdoptersResponses,
  model: transformSnakeToCamel(mockProjectAdoptersResponses)
};

/** 
  * Fetches project mocks for a particular working group.
  * @param {string} workingGroup - The working group id to get project mocks
  * for.
  */
export const getMockProjectsFromWorkingGroup = (workingGroup) => {
  const projectIds = Array.from(workingGroupProjects[workingGroup]);

  const mockProjectAdoptersResponses = mockProjectAdopters.api.filter(project => projectIds.includes(project.project_id))

  return {
    api: mockProjectAdoptersResponses,
    model: transformSnakeToCamel(mockProjectAdoptersResponses)
  };
}
