/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

export const mockJakartaEEOrganizations = [
  {
    organization_id: 1309,
    name: 'London Jamocha Community',
    member_since: null,
    description: {
      long: 'This is London Jamocha Community\'s long description.',
    },
    logos: {
      print: null,
      web: 'https://membership.eclipse.org/organization/images/1309-web.gif',
    },
    website: 'https://www.meetup.com/Londonjavacommunity/',
    levels: [
      {
        level: 'AP',
        description: 'Contributing Member',
        sort_order: '200',
      }
    ],
    wgpas: [
      {
        document_id: 'abcdefg1234',
        description: 'Participant Member',
        level: 'WGAPS',
        working_group: 'jakarta-ee',
      }
    ]
  },
  {
    organization_id: 1312,
    name: 'Istanbul JUG',
    member_since: '2018-05-02',
    description: {
      long: 'This is Istanbul JUG\'s long description.',
    },
    logos: {
      print: null,
      web: 'https://membership.eclipse.org/organization/images/1312-web.gif',
    },
    website: 'https://www.jugistanbul.org/',
    levels: [
      {
        level: 'AS',
        description: 'Associate Member',
        sort_order: '100',
      },
    ],
    wgpas: [
      {
        document_id: 'asdfghj1234',
        description: 'Guest Member',
        level: 'WGSAP',
        working_group: 'jakarta-ee',
      }
    ]
  }
];

export const mockOrganizations = [
  {
    organization_id: 656,
    name: 'IBM',
    member_since: null,
    description: {
      long: 'This is IBM\'s long description.',
    },
    logos: {
      print: null,
      web: 'https://membership.eclipse.org/organization/images/656-web.gif',
    },
    website: 'https://www.ibm.com',
    levels: [
      {
        level: 'SD',
        description: 'Strategic Member',
        sort_order: '500',
      },
    ],
    wgpas: [
      {
        document_id: '27f8dsf9s8adf',
        description: null,
        level: null,
        working_group: 'unknown',
      },
    ],
  },
  ...mockJakartaEEOrganizations,
];

export const mockOrganizationsByWorkingGroup = {
  'jakarta-ee': mockJakartaEEOrganizations,
};

export const mockOrganizationsByProject = {
  'ee4j': mockJakartaEEOrganizations,
};
