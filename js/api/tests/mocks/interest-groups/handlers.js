/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { rest } from 'msw';
import { mockInterestGroups } from './mock-data';

const apiUrl = 'https://projects.eclipse.org/api/interest-groups';

const handlers = [
  rest.get(`${apiUrl}`, (_req, res, ctx) => {
    return res(
      ctx.status(200), 
      ctx.json(mockInterestGroups.api)
    );
  }),

  rest.get(`${apiUrl}/:interestGroupNodeId`, (req, res, ctx) => {
    const interestGroupNodeId = parseInt(req.params.interestGroupNodeId);
    const interestGroup = mockInterestGroups.api.find(interestGroup => interestGroup.id === interestGroupNodeId);

    return res(
      ctx.status(200),
      ctx.json(interestGroup)
    );
  }),
];

export default handlers;
