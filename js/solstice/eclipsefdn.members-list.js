/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import $ from 'jquery';
import 'jquery-match-height';
import getMembers from '../api/eclipsefdn.members';
import { displayErrMsg } from './utils';
import { validateURL } from '../api/utils';
import template from './templates/explore-members/member.mustache';
import templateOnlyLogos from './templates/wg-members/wg-member-only-logos.mustache';
import templateLogoTitleWithLevels from './templates/wg-members/wg-member-logo-title-with-levels.mustache';
import templateLoading from './templates/loading-icon.mustache';
import { getInterestGroupParticipatingOrganizations } from '../api/eclipsefdn.interest-groups';
import { organizationMapper, getProjectParticipatingOrganizations } from '../api/eclipsefdn.membership';

const SD_NAME = 'Strategic Members';
const AP_NAME = 'Contributing Members';
const AS_NAME = 'Associate Members';

const defaultOptions = {
  mlWg: null,
  mlLevel: null,
  mlSort: null,
  mlLinkMemberWebsite: null,
  type: 'working-group',
  id: null,
};

const render = async () => {
  const elements = document.querySelectorAll('.eclipsefdn-members-list');
  
  elements.forEach(async (element) => {
    element.innerHTML = templateLoading();

    let membersListArray = [
      { level: SD_NAME, members: [] },
      { level: AP_NAME, members: [] },
      { level: AS_NAME, members: [] },
    ];
    let url = 'https://membership.eclipse.org/api/organizations?pagesize=100';

    const options = { ...defaultOptions, ...element.dataset };

    const isRandom = options.mlSort === 'random';
    const level = options.mlLevel;
    const linkMemberWebsite = element.getAttribute('data-ml-link-member-website') === 'true' ? true : false;
    
    // The id of a resource of type `options.type` (ex. working-group, interest-group, project).
    const id = options.id || options.mlWg;

    if (level) {
      level.split(' ').forEach((item) => (url = `${url}&levels=${item}`));
    }

    let membersData = [];
    if (options.type === 'working-group') {
      if (id) { 
        url = `${url}&working_group=${id}`;
        membersListArray = [{ level: '', members: [] }];
      }
      // When we show members belong to a wg, no need to show EF levels
      membersData = (await getMembers(url, [], (err) => displayErrMsg(element, err)))
        .map(organizationMapper);
    } else if (options.type === 'interest-group') {
      membersListArray = [{ level: '', members: [] }];

      // We assume the interest group id is a Drupal node ID if it is numeric
      // This widget needs to support node id and short ids for interest groups
      const isNodeId = !Number.isNaN(parseInt(id));
      [membersData] = await getInterestGroupParticipatingOrganizations({ 
        interestGroupId: isNodeId ? undefined : id,
        interestGroupNodeId: isNodeId ? id : undefined
      });
    } else if (options.type === 'project') {
      membersListArray = [{ level: '', members: [] }];
      [membersData] = await getProjectParticipatingOrganizations(id);
    }

    if (membersData.length === 0) {
      element.innerHTML = `<p class="members-list-info-text">No members to display.</p>`;
      return;  
    }

    const addMembersWithoutDuplicates = (levelName, memberData) => {
      // When we show members belong to a wg, only 1 item exits in membersListArray
      const currentLevelMembers = id ? membersListArray[0] : membersListArray.find((item) => item.level === levelName);
      const isDuplicatedMember = currentLevelMembers.members.find(
        (item) => item.organizationId === memberData.organizationId
      );
      !isDuplicatedMember && currentLevelMembers.members.push(memberData);
    };

    membersData = membersData.map((member) => {
      if (!member.name) {
        // will not add members without a title/name into membersListArray to display
        return member;
      }

      if (member.levels.find((item) => item.level?.toUpperCase() === 'SD')) {
        addMembersWithoutDuplicates(SD_NAME, member);
        return member;
      }

      if (member.levels.find((item) => item.level?.toUpperCase() === 'AP' || item.level?.toUpperCase() === 'OHAP')) {
        addMembersWithoutDuplicates(AP_NAME, member);
        return member;
      }

      if (member.levels.find((item) => item.level?.toUpperCase() === 'AS')) {
        addMembersWithoutDuplicates(AS_NAME, member);
        return member;
      }

      return member;
    });

    if (isRandom) {
      // Sort randomly
      membersListArray.forEach((eachLevel) => {
        let tempArray = eachLevel.members.map((item) => item);
        const randomItems = [];
        eachLevel.members.forEach(() => {
          const randomIndex = Math.floor(Math.random() * tempArray.length);
          randomItems.push(tempArray[randomIndex]);
          tempArray.splice(randomIndex, 1);
        });
        eachLevel.members = randomItems;
      });
    } else {
      // Sort alphabetically by default
      membersListArray.forEach((eachLevel) => {
        eachLevel.members.sort((a, b) => {
          const preName = a.name.toUpperCase();
          const nextName = b.name.toUpperCase();
          if (preName < nextName) {
            return -1;
          }
          if (preName > nextName) {
            return 1;
          }
          return 0;
        });
      });
    }

    if (level) {
      membersListArray = membersListArray.filter((list) => list.members.length !== 0);
    }

    const urlLinkToLogo = function () {
      if (linkMemberWebsite && validateURL(this.website)) {
        return this.website;
      }
      return `https://www.eclipse.org/membership/showMember.php?member_id=${this.organizationId}`;
    };

    const displayMembersByLevels = async (theTemplate) => {
      if (options.type !== 'working-group') {
        console.error('Only "working-group" type is supported for displaying members by level at this time');
        return;
      }
      
      const allWGData = await (await fetch('https://membership.eclipse.org/api/working_groups')).json();
      let currentWGLevels = allWGData.find((item) => item.alias === id).levels;
      // defaultLevel is for members without a valid level. So far, only occurs on IoT.
      const defaultLevel = element.getAttribute('data-ml-default-level');
      const specifiedLevel = element.getAttribute('data-ml-wg-level');
      if (specifiedLevel) {
        currentWGLevels = currentWGLevels.filter((level) =>
          specifiedLevel.toLowerCase().includes(level.relation.toLowerCase())
        );
      }
      element.innerHTML = '';
      if (defaultLevel) {
        currentWGLevels.push({ relation: 'default', description: defaultLevel, members: [] });
      }

      // categorize members into corresponding levels
      currentWGLevels.forEach((level) => {
        level['members'] = [];
        membersListArray[0].members.forEach((member) => {
          const memberLevel = member.wgpas.find((wgpa) => wgpa.workingGroup === id)?.level;
          if (memberLevel === level.relation) {
            level.members.push(member);
          }
          if (level.relation === 'default' && memberLevel === null) {
            level.members.push(member);
          }
        });

        if (level.members.length === 0) {
          return;
        }

        const showLevelUnderLogo = () => {
          if (
            !element.getAttribute('data-ml-level-under-logo') ||
            element.getAttribute('data-ml-level-under-logo') === 'false'
          ) {
            return false;
          }
          return level.description.replaceAll(' Member', '');
        };

        element.innerHTML =
          element.innerHTML +
          theTemplate({ item: level.members, levelDescription: level.description, urlLinkToLogo, showLevelUnderLogo });
      });
    };

    switch (element.getAttribute('data-ml-template')) {
      case 'only-logos':
        element.innerHTML = templateOnlyLogos({
          item: membersListArray[0].members,
          urlLinkToLogo,
        });
        return;

      case 'logo-title-with-levels':
        await displayMembersByLevels(templateLogoTitleWithLevels);
        $.fn.matchHeight._applyDataApi();
        return;
      case 'logo-with-levels':
        displayMembersByLevels(templateOnlyLogos);
        return;
      default:
        break;
    }

    element.innerHTML = template({
      sections: membersListArray,
      hostname: window.location.hostname.includes('staging.eclipse.org')
        ? 'https://staging.eclipse.org'
        : 'https://www.eclipse.org',
    });
    $.fn.matchHeight._applyDataApi();
  });
};

export default { render };
