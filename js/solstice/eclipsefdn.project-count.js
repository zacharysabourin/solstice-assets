/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
  *  Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { getProjectCount } from '../api/eclipsefdn.projects';

/**
  * @typedef {Object} Options
  * @property {string} [collaboration] The collaboration to filter projects
  * by. If not provided, all projects from the Eclipse Foundation are counted.
  */

/** @type {Options} */
const defaultOptions = {
  collaboration: null,
}

/** 
  * The render function for project count.
  */
const render = async () => {
  const element = document.querySelector('.eclipsefdn-project-count');
  if (!element) return;

  /** @type {Options} */
  const options = { ...defaultOptions, ...element.dataset };

  const [projectCount, error] = await getProjectCount(options.collaboration);
  // Clear contents if error.
  if (error) { 
    element.innerHTML = ''
    return;
  };
  
  // Render project count.
  element.innerHTML = projectCount;
};

export default { render };
