/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

// Mega Menu
const hideMegaMenu = () => {
  const headerElement = document.querySelector('header');

  const primaryMenuItemElements = Array.from(headerElement.querySelectorAll('.nav-link-js'));

  primaryMenuItemElements.forEach(primaryMenuItemElement => {
    primaryMenuItemElement.classList.remove('nav-link-active');
    primaryMenuItemElement.setAttribute('aria-expanded', false);
  });

  const menuElements = Array.from(headerElement.querySelectorAll('.mega-menu-submenu'));

  menuElements.forEach(menuElement => {
    menuElement.classList.add('hidden');
  });
};

const setCurrentMegaMenu = menuId => {
  const nextMenuElement = document.querySelector(`header [data-menu-id=${menuId}]`);
  if (!nextMenuElement) return;

  // Navbar
  const nextPrimaryMenuItemElement = document.querySelector(
    `header .nav-link-js[data-menu-target=${menuId}]`
  );
  const prevPrimaryMenuItemElement = document.querySelector('header .nav-link-active');
  const prevMenuId = prevPrimaryMenuItemElement?.dataset.menuTarget;

  // Only show next menu unless it was already active
  hideMegaMenu();

  if (!prevMenuId || prevMenuId !== menuId) {
    nextMenuElement.classList.remove('hidden');
    nextPrimaryMenuItemElement.classList.add('nav-link-active');
    nextPrimaryMenuItemElement.setAttribute('aria-expanded', true);
    return;
  }
};

const handleMegaMenuNavItemClick = event => {
  const navItemElement = event.target;
  if (!navItemElement) return;

  const targetMenuId = navItemElement.dataset.menuTarget;
  if (!targetMenuId) {
    console.error(`Nav item "${navItemElement.textContent.trim()}" is missing menu target`);
    return;
  }

  setCurrentMegaMenu(targetMenuId);
};

// Mobile Menu
const handleMobileMenuButtonClick = () => {
  const mobileMenuElement = document.querySelector('header .mobile-menu');
  mobileMenuElement.classList.toggle('hidden')
}

const handleMobileMenuDropdownClick = event => {
  const dropdownToggleElement = event.target;
  if (!dropdownToggleElement) return;

  // Get the target dropdown menu from the menu item that was clicked. Then toggle the dropdown's
  // visibility.
  const toggleTargetId = dropdownToggleElement.dataset.target;
  if (!toggleTargetId) {
    console.error(`No toggle target id for mobile menu item`);
    return;
  }
  const targetDropdownElement = document.getElementById(toggleTargetId);
  if (!targetDropdownElement) {
    console.error(`Could not find toggle target of id "${toggleTargetId}"`);
    return;
  }

  targetDropdownElement.classList.toggle('hidden');
};

const handleClickOutsideHeader = event => {
  const megaMenuElement = document.querySelector('header');
  const isOuterClick = !megaMenuElement.contains(event.target);

  if (isOuterClick) {
    hideMegaMenu();
  }
};

const applyEventHandlers = headerElement => {
  // Mobile menu events
  const mobileMenuButtonElement = document.querySelector('header .mobile-menu-btn');
  mobileMenuButtonElement.addEventListener('click', handleMobileMenuButtonClick);

  const dropdownToggleElements = Array.from(
    headerElement.querySelectorAll('.mobile-menu-dropdown-toggle')
  );

  dropdownToggleElements.forEach(dropdownToggleElement => {
    dropdownToggleElement.addEventListener('click', handleMobileMenuDropdownClick);
  });

  // Mega menu events
  const navItemElements = Array.from(headerElement.querySelectorAll('.nav-link-js'));

  navItemElements.forEach(navItemElement => {
    navItemElement.addEventListener('click', handleMegaMenuNavItemClick);
  });

  document.addEventListener('click', handleClickOutsideHeader);
};

const run = () => {
  const headerElement = document.querySelector('header');
  if (!headerElement) return;

  applyEventHandlers(headerElement);
};

export default { run };
