/**
 * @jest-environment jsdom
 */

/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import eclipsefdnProjectCount from '../eclipsefdn.project-count';
import * as projectsModule from '../../api/eclipsefdn.projects';
import { waitForRender } from '../utils';

describe('eclipsefdn.project-count', () => {
  let getProjectsDataSpy;

  beforeEach(() => {
    getProjectCountSpy = jest.spyOn(projectsModule, 'getProjectCount');
  });

  it('should render the project count', async () => {
    getProjectCountSpy.mockReturnValue([6, null]);
    document.querySelector('body').innerHTML = `
      <p 
        class="eclipsefdn-project-count" 
      >
      </p>
    `;
    eclipsefdnProjectCount.render();

    const projectCountElement = document.querySelector('.eclipsefdn-project-count');
    await waitForRender(projectCountElement);

    expect(projectCountElement.innerHTML).toBe('6');

    getProjectCountSpy.mockRestore();
  });

  it('should render the project count with a collaboration filter', async () => {
    getProjectCountSpy.mockReturnValue(Array.from([3, null]));
    document.querySelector('body').innerHTML = `
      <p
        class="eclipsefdn-project-count"
        data-collaboration="eclipse-ide"
      >
      </p>
    `;
    eclipsefdnProjectCount.render();

    const projectCountElement = document.querySelector('.eclipsefdn-project-count');
    await waitForRender(projectCountElement);

    expect(projectCountElement.innerHTML).toBe('3');
    expect(getProjectCountSpy).toHaveBeenCalledWith('eclipse-ide');

    getProjectCountSpy.mockRestore();
  });
});
