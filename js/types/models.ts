/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

//== Adopter Models
export interface Adopter {
  name: string;
  logo: {
    default: string | null;
    white: string | null;
  };
  projects: string[];
  homepageUrl: string;
}

export interface ProjectWithAdopters {
  projectId: string;
  name: string;
  url: string;
  logo: string;
  adopters: Adopter[];
}

//== Newsroom Models

export interface Ad {
  /** The ad's ID. */
  id: number;
  /** The campaign ID (aka. name). */
  campaignName: number;
  /** The ad's format. (e.g. 'ads_square') */
  format: 'ads_square' | 'ads_download' | 'ads_top_leaderboard' | 'ads_leaderboard';
  /** The ad's image url. */
  image: string;
  /** An array of publishing destinations (e.g. 'eclipse_org_home'). */
  publishTo: string[];
  /** The ad's url. */
  url: string;
}

export interface NewsroomResourceAuthor {
  fullName: string;
}

export interface NewsroomResource {
  /** The resource ID. */
  id: number;
  /** The resource title. */
  title: string;
  /** The resource date. */
  date: Date;
  /** The resource type. */
  type: 'Case Study' | 'Market Report' | 'Survey Report' | 'Social Media Kit' | 'White Paper';
  /** The resource thumbnail image. */
  image: string;
  /** The resource author. */
  author: NewsroomResourceAuthor;
  /** An array of publishing destinations (e.g. 'eclipse_org'). */
  publishTo: string[];
  /** The resource's body text **/
  body: string;
  /** The resource's direct link. **/
  directLink: string;
  /** The resource's landing link. **/
  landingLink?: string;
}

//== Membership Models

export interface ParticipationAgreement {
  documentId: string;
  description: string | null;
  level: string | null;
  workingGroup: string;
}

type OrganizationLevel = 'SD' | 'OHAP' | 'AP' | 'AS';

export interface OrganizationLevelDetailed {
  level: OrganizationLevel;
  description: string;
  sortOrder: number;
}

export interface Organization {
  organizationId: number;
  name: string;
  description: {
    long: string;
  } | null;
  logos: {
    print: string | null;
    web: string | null;
  },
  website: string | null;
  levels: OrganizationLevelDetailed[];
  participationAgreements: ParticipationAgreement[];
}

