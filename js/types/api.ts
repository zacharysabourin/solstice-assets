/*
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

/** Class representing an HTTP error. It can be thrown to indicate that an HTTP
  * error occurred. 
  */
export class HttpError extends Error {
  public code: number;
  public message: string;

  /** 
    * @param code - The HTTP status code.
    * @param message - The error message.  
    */
  constructor(code: number, message: string) {
    super(`HTTP Error: ${code} ${message}`);
    this.code = code;
    this.message = message;
    this.name = 'HTTPError';
  }
}


export interface APIResponseError {
  /** The HTTP status code **/
  code?: number;
  /** The error message **/
  message: string;
}

/** The APIResponse type is a tuple of the data and error. */
export type APIResponse<T> = [T | null, APIResponseError | null];

